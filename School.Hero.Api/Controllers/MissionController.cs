﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Hero.Api.Models;
using School.Hero.Api.Models.ViewModels;
using School.Hero.Api.Repository.Wrapper;

namespace School.Hero.Api.Controllers
{
    [Route("api/Mission")]
    [ApiController]
    public class MissionController : BaseApiController
    {
        private IRepositoryWrapper _repo;

        public MissionController(IRepositoryWrapper repo)
        {
            _repo = repo;
        }

        // GET: api/Mission
        /// <summary>
        /// Lista todas as Missões ativas
        /// </summary>
        /// <returns>Retorna todas as missões ativas</returns>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var modelData = _repo.Mission.GetAll().Where(x => x.IsActive).ToList();

                return Ok(ModelFactory.Convert(modelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }
        }

        // GET: api/Mission/{Id}
        /// <summary>
        /// Busca uma Missão pelo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Retorna uma missão</returns>
        [HttpGet("{Id:int}")]
        public IActionResult Get(int Id)
        {
            try
            {
                Mission Mission = _repo.Mission.GetById(Id);

                if (Mission == null)
                {
                    return NotFound("Mission not found.");
                }

                return Ok(ModelFactory.Convert(Mission));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/Mission
        /// <summary>
        /// Salva um nova Missão
        /// </summary>
        /// <param name="Mission"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] MissionModel Mission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var modelData = ModelFactory.Convert(Mission);

                modelData.IsActive = true;//Seta a missao como ativa
                modelData.IsCompleted = false;//Seta a missao como não completada
                modelData.DateCreation = DateTime.Now;

                _repo.Mission.Insert(modelData);

                return Ok(ModelFactory.Convert(_repo.Mission.GetById(modelData.MissionId)));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/Mission/{Id}
        /// <summary>
        /// Atualiza uma Missão que esteja ativa
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Mission"></param>
        /// <returns></returns>
        [HttpPut("{Id:int}")]
        public IActionResult Put(int Id, [FromBody] MissionModel Mission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //Busca a missao na base
                Mission MissionToUpdate = _repo.Mission.GetById(Id);

                if (MissionToUpdate == null)
                {
                    return NotFound("Mission not found.");
                }

                if (!MissionToUpdate.IsActive)
                {
                    return BadRequest("Missão inativa");
                }

                //Mergeia a missao da api com a missao da base
                ModelFactory.Merge(ref MissionToUpdate, Mission);

                //Salva o Usuario
                _repo.Mission.Update(MissionToUpdate);

                //Cria historico
                string teste = $"Missão {Id} completada";
                _repo.History.CreateHistory(teste);

                return Ok(ModelFactory.Convert(MissionToUpdate));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
                throw;
            }


        }

        //POST api/Mission/{id}/CompleteMission
        /// <summary>
        /// Completa uma Missão
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("{Id:int}/CompleteMission")]
        public IActionResult CompleteMission(int Id)
        {
            try
            {
                var ModelData = _repo.Mission.GetById(Id);

                if (ModelData == null)
                {
                    return BadRequest("Missão não encontrada");
                }

                ModelData.IsCompleted = true;

                _repo.Mission.Update(ModelData);

                //Cria historico
                string teste = $"Missão {ModelData.MissionId} completada";

                _repo.History.CreateHistory(teste);

                return Ok("Missão Encerrada");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        //POST api/Mission/{id}/DeleteMission
        /// <summary>
        /// Deleta uma Missão
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("{Id:int}/DeleteMission")]
        public IActionResult Delete(int Id)
        {
            try
            {
                var ModelData = _repo.Mission.GetById(Id);

                if (ModelData == null)
                {
                    return BadRequest("Missão nao encontrada");
                }

                ModelData.IsActive = false;

                _repo.Mission.Update(ModelData);

                string teste = $"Missão {ModelData.MissionId} deletada";

                _repo.History.CreateHistory(teste);

                return Ok("Missão Deletada");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("{Id:int}/RewardMission")]
        public IActionResult RewardMission(int Id, [FromBody]List<int> UserIds)
        {
            try
            {
                var mission = _repo.Mission.GetById(Id);

                if (!mission.IsActive)
                {
                    return BadRequest("Esta missão foi excluida.");
                }

                //se a missao nao estiver completada nao permite dar moedas
                if (!mission.IsCompleted)
                {
                    return BadRequest("A missão ainda esta ativa.");
                }

                var users = _repo.User.GetByIds(UserIds);

                foreach (var user in users)
                {
                    user.Experience += mission.Experience;   //acrescenta a exp da missao ao usuario
                    user.Coins += mission.Value;             //Acrescenta o valor da missao aos coins do usuario

                    _repo.User.Update(user);
                }

                return Ok();

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Hero.Api.Models;
using School.Hero.Api.Models.ViewModels;
using School.Hero.Api.Repository.Wrapper;

namespace School.Hero.Api.Controllers
{
    [Route("api/Users")]
    [ApiController]
    public class UsersController : BaseApiController
    {
        private IRepositoryWrapper _repo;

        public UsersController(IRepositoryWrapper repo)
        {
            _repo = repo;
        }

        // GET: api/Users
        /// <summary>
        /// Lista todos os usuarios ativos
        /// </summary>
        /// <returns>UserModel</returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                IList<User> Users = _repo.User.GetAll().Where(x => x.IsActive).ToList();

                return Ok(ModelFactory.Convert(Users));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // GET: api/Users/5
        /// <summary>
        /// Busca um usuario pelo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id}")]
        public IActionResult Get(int Id)
        {
            try
            {
                User User = _repo.User.GetById(Id);

                if (User == null)
                {
                    return NotFound("User not found.");
                }

                return Ok(ModelFactory.Convert(User));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/Users
        /// <summary>
        /// Cria um novo usuario
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] UserModel User)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                User data = _repo.User.GetByUserName(User.Username);

                if (data == null)
                {
                    return BadRequest("Usuario ja existente");
                }


                User UserConvert = ModelFactory.Convert(User);

                _repo.User.Insert(UserConvert);

                return Ok(ModelFactory.Convert(UserConvert));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }

        // PUT: api/Users/5
        /// <summary>
        /// Atualiza um usuario que esteja Ativo
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="User"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        public IActionResult Put(int Id, [FromBody] UserModel User)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //Busca a pessoa na base
                User UserToUpdate = _repo.User.GetById(Id);

                if (UserToUpdate == null)
                {
                    return NotFound("User not found.");
                }

                //Mergeia a pessoa da api com a pessoa da base
                ModelFactory.Merge(ref UserToUpdate, User);

                //Salva o Usuario
                _repo.User.Update(UserToUpdate);

                return Ok(ModelFactory.Convert(UserToUpdate));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
                throw;
            }

        }

        // DELETE: api/Users/5
        /// <summary>
        /// Inativa o usuario
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("{id}/DeleteUser")]
        public IActionResult Delete(int Id)
        {
            try
            {
                var ModelData = _repo.User.GetById(Id);

                if (ModelData == null)
                {
                    return NotFound("Usuario não encontrada");
                }

                ModelData.IsActive = false;

                _repo.User.Update(ModelData);

                //Cria historico
                string action = $"Usuario {Id} excluido";
                _repo.History.CreateHistory(action);

                return Ok("Usuario excluído");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Usado para definir uma missao para o usuario
        /// </summary>
        /// <remarks>
        /// api/Users/{UserId}/CreateMission?MissionId={MissionId}
        /// </remarks>
        /// <param name="UserId"></param>
        /// <param name="MissionId"></param>
        /// <returns></returns>
        [HttpPost("{UserId:int}/AcceptMission")]
        public IActionResult CreateMission(int UserId, int MissionId)
        {
            try
            {
                var user = _repo.User.GetById(UserId);

                if (user == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                var mission = _repo.Mission.GetById(MissionId);
                if (mission == null)
                {
                    return NotFound("Missão não encontrada");
                }

                //Se a missao estiver completa nao permite relacionar
                if (mission.IsCompleted)
                {
                    return BadRequest("Esta missão ja terminou");
                }

                if (!mission.IsActive)
                {
                    return BadRequest("Esta missão foi excluida");
                }

                Mission_User modelData = new Mission_User()
                {
                    MissionId = MissionId,
                    UserId = UserId
                };

                _repo.Mission_User.Insert(modelData);

                return Ok();

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Usado para definir uma recompensa para o usuario
        /// </summary>
        /// <remarks>
        /// api/Users/{UserId}/CreateShop?MissionId={MissionId}
        /// </remarks>
        /// <param name="UserId"></param>
        /// <param name="ShopId"></param>
        /// <returns></returns>
        [HttpPost("{UserId:int}/BuyFromShop")]
        public IActionResult CreateShop(int UserId, int ShopId)
        {
            try
            {
                var user = _repo.User.GetById(UserId);

                if (user == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                var shop = _repo.Shop.GetById(ShopId);
                if (shop == null)
                {
                    return NotFound("Shop não encontrada");
                }

                if (!shop.IsActive)
                {
                    return BadRequest("Esta recompensa ja encerrou");
                }


                User_Shop modelData = new User_Shop()
                {
                    ShopId = ShopId,
                    UserId = UserId
                };

                _repo.User_Shop.Insert(modelData);

                return Ok();

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Login de usuario da aplicação
        /// </summary>
        /// <param name="Login"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public IActionResult Login(LoginModel Login)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Login.Username))
                {
                    return NotFound("Usuario ou senha invalidos");
                }

                if (string.IsNullOrWhiteSpace(Login.Password))
                {
                    return NotFound("Usuario ou senha invalidos");
                }

                var user = _repo.User.Login(Login.Username, Login.Password);

                if (user == null)
                {
                    return NotFound("Usuario ou senha invalidos");
                }

                //Busca a permissão do usuario
                User_Permission userPermisions = _repo.User_Permission.GetByUserId(user.UserId);

                UserPermissionModel result = new UserPermissionModel()
                {
                    UserId = user.UserId,
                    Permission = ModelFactory.Convert(_repo.Permission.GetById(userPermisions.PermissionId))
            };

                return Ok(result);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo usado para dar moedas
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost("{Id:int}/SendCoins")]
        public IActionResult SendCoins(int Id, [FromBody]CoinsModel Model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var ModelData = _repo.User.GetById(Id);

                if (ModelData == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                //Incrementa os coins mandados
                ModelData.Coins += Model.Coins;

                _repo.User.Update(ModelData);

                //Cria historico
                string action = $"Adicionados {Model.Coins} Moedas ao usuario {ModelData.Username}";
                _repo.History.CreateHistory(action, Model.Reason);

                return Ok(new { Sucess = true, Message = "Moedas adicionadas" });

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Metodo responsavel por retirar coins
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpPost("{Id:int}/RemoveCoins")]
        public IActionResult RemoveCoins(int Id, [FromBody]CoinsModel Model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var ModelData = _repo.User.GetById(Id);

                if (ModelData == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                //Incrementa os coins mandados
                ModelData.Coins -= Model.Coins;

                _repo.User.Update(ModelData);

                //Cria historico
                string action = $"Removidos {Model.Coins} Moedas do usuario {ModelData.Username}";
                _repo.History.CreateHistory(action, Model.Reason);

                return Ok(new { Sucess = true, Message = "Moedas Removidas" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Retorna as missões que o usuario aceitou e que estao ativas
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id:int}/Missions")]
        public IActionResult GetMissions(int Id)
        {
            try
            {
                var User = _repo.User.GetById(Id);

                if (User == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                var userMission = _repo.Mission_User.GetByUser(Id);

                var Missions = _repo.Mission.GetByIds(userMission.Select(x => x.MissionId).ToList());

                return Ok(ModelFactory.Convert(Missions));

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Retorna as recompensas que o usuario comprou
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id:int}/Shop")]
        public IActionResult GetShop(int Id)
        {
            try
            {
                var User = _repo.User.GetById(Id);

                if (User == null)
                {
                    return NotFound("Usuario nao encontrado");
                }

                var userShop = _repo.User_Shop.GetByUser(Id);

                var Missions = _repo.Shop.GetByIds(userShop.Select(x => x.ShopId).ToList());

                return Ok(ModelFactory.Convert(Missions));

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Hero.Api.Models;
using School.Hero.Api.Models.ViewModels;
using School.Hero.Api.Repository.Wrapper;

namespace School.Hero.Api.Controllers
{
    [Route("api/Permission")]
    [ApiController]
    public class PermissionController : BaseApiController
    {
        private IRepositoryWrapper _repo;

        public PermissionController(IRepositoryWrapper repo)
        {
            _repo = repo;
        }

        // GET: api/Permission
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var modelData = _repo.Permission.GetAll().ToList();

                return Ok(ModelFactory.Convert(modelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }
        }

        // GET: api/Permission/5
        [HttpGet("{Id}")]
        public IActionResult Get(int Id)
        {
            try
            {
                var modelData = _repo.Permission.GetById(Id);

                if (modelData == null)
                {
                    return NotFound("Mission not found.");
                }

                return Ok(ModelFactory.Convert(modelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/Permission
        [HttpPost]
        public IActionResult Post([FromBody] PermissionModel ModelView)
        {
            if (ModelView == null)
            {
                return BadRequest("Mission is null.");
            }

            try
            {
                _repo.Permission.Insert(ModelFactory.Convert(ModelView));

                return CreatedAtRoute("Get", new { Id = ModelView.PermissionId }, ModelView);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/Permission/5
        [HttpPut("{Id}")]
        public IActionResult Put(int Id, [FromBody] PermissionModel ModelView)
        {
            if (ModelView == null)
            {
                return BadRequest("Mission is null.");
            }

            try
            {
                //Busca a pessoa na base
                var ModelToUpdate = _repo.Permission.GetById(Id);

                if (ModelToUpdate == null)
                {
                    return NotFound("Mission not found.");
                }

                //Mergeia a missao da api com a missao da base
                ModelFactory.Merge(ref ModelToUpdate, ModelView);

                //Salva o Usuario
                _repo.Permission.Update(ModelToUpdate);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
                throw;
            }

            return CreatedAtRoute("Get", new { Id }, ModelView);
        }

        // DELETE: api/Permission/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var ModelData = _repo.Permission.GetById(id);

            if (ModelData == null)
            {
                return NotFound("Mission not found.");
            }

            try
            {
                _repo.Permission.Delete(ModelData);

                return StatusCode(200, "Sucess");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
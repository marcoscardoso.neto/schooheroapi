﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using School.Hero.Api.Models;
using School.Hero.Api.Models.ViewModels;
using School.Hero.Api.Repository.Wrapper;

namespace School.Hero.Api.Controllers
{
    [Route("api/Shop")]
    [ApiController]
    public class ShopController : BaseApiController
    {
        private IRepositoryWrapper _repo;

        public ShopController(IRepositoryWrapper repo)
        {
            _repo = repo;
        }

        // GET: api/Shop
        /// <summary>
        /// Lista todas as recompensar ativas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                //busca todos as recompensas que estao ativas
                var modelData = _repo.Shop.GetAll().Where(x=> x.IsActive).ToList();

                return Ok(ModelFactory.Convert(modelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }
        }

        // GET: api/Shop/5
        /// <summary>
        /// Lista a recompensa pelo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet("{Id}")]
        public IActionResult Get(int Id)
        {
            try
            {
                var modelData = _repo.Shop.GetById(Id);

                if (modelData == null)
                {
                    return NotFound("Mission not found.");
                }

                return Ok(ModelFactory.Convert(modelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // POST: api/Shop
        /// <summary>
        /// Cria uma nova recompensa
        /// </summary>
        /// <param name="ModelView"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] ShopModel ModelView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var ModelData = ModelFactory.Convert(ModelView);

                //seta o shop como ativo
                ModelData.IsActive = true;

                _repo.Shop.Insert(ModelData);

                return Ok(ModelFactory.Convert(ModelData));
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // PUT: api/Shop/5
        /// <summary>
        /// Atualiza uma recompensa
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="ModelView"></param>
        /// <returns></returns>
        [HttpPut("{Id}")]
        public IActionResult Put(int Id, [FromBody] ShopModel ModelView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //Busca a pessoa na base
                var ModelToUpdate = _repo.Shop.GetById(Id);

                if (ModelToUpdate == null)
                {
                    return NotFound("Mission not found.");
                }

                //Mergeia a missao da api com a missao da base
                ModelFactory.Merge(ref ModelToUpdate, ModelView);

                //Salva o Usuario
                _repo.Shop.Update(ModelToUpdate);

                return Ok(ModelFactory.Convert(ModelToUpdate));

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Deleta a recompensa
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost("{Id:int}/DeleteShop")]
        public IActionResult DeleteShop(int Id)
        {
            try
            {
                var ModelData = _repo.Shop.GetById(Id);

                if (ModelData == null)
                {
                    return BadRequest("Recompensa não encontrada");
                }

                ModelData.IsActive = false;

                _repo.Shop.Update(ModelData);

                return Ok("Recompensa finalizada");

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public User GetById(int Id)
        {
            return RepositoryContext.Users.Find(Id);
        }

        public User Login(string UserName, string Password)
        {
            var linq = from u in RepositoryContext.Users
                       where u.Username.Equals(UserName) && u.Password.Equals(Password)
                       select u;

            return linq.FirstOrDefault();
        }

        public User GetByUserName(string UserName)
        {
            var linq = from u in RepositoryContext.Users
                       where u.Username.Equals(UserName)
                       select u;

            return linq.FirstOrDefault();
        }

        public IList<User> GetByIds(List<int> Ids)
        {
            var linq = from u in RepositoryContext.Users
                       where Ids.Contains(u.UserId) && u.IsActive
                       select u;

            return linq.ToList();
        }

    }
}

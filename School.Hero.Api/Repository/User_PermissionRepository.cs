﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class User_PermissionRepository : BaseRepository<User_Permission>, IUser_PermissionRepository
    {
        public User_PermissionRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public User_Permission GetById(int Id)
        {
            return RepositoryContext.User_Permissions.Find(Id);
        }

        public User_Permission GetByUserId(int UserId)
        {
            var linq = from up in RepositoryContext.User_Permissions
                       where up.UserId.Equals(UserId)
                       select up;

            return linq.OrderBy(x=> x.PermissionId).FirstOrDefault();
        }
    }
}

﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class ShopRepository : BaseRepository<Shop>, IShopRepository
    {
        public ShopRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public Shop GetById(int Id)
        {
            return RepositoryContext.Shops.Find(Id);
        }

        public IList<Shop> GetByIds(List<int> Ids)
        {
            var linq = from m in RepositoryContext.Shops
                       where Ids.Contains(m.ShopId)
                       select m;

            return linq.ToList();
        }
    }
}

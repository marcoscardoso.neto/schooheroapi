﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace School.Hero.Api.Repository
{
    public class MissionRepository : BaseRepository<Mission>, IMissionRepository
    {
        public MissionRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public Mission GetById(int Id)
        {
            return RepositoryContext.Missions.Find(Id);
        }

        public IList<Mission> GetByIds(List<int> Ids)
        {
            var linq = from m in RepositoryContext.Missions
                       where Ids.Contains(m.MissionId) && m.IsActive
                       select m;

            return linq.ToList();
        }
    }
}

﻿using School.Hero.Api.Repository.Contracts;
using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Hero.Api.Models.Context;

namespace School.Hero.Api.Repository
{
    public class Mission_UserRepository : BaseRepository<Mission_User>, IMission_UserRepository
    {
        public Mission_UserRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public Mission_User GetById(int Id)
        {
            return RepositoryContext.Mission_Users.Find(Id);
        }

        public IList<Mission_User> GetByUser(int Id)
        {
            var linq = from mu in RepositoryContext.Mission_Users
                       where mu.UserId.Equals(Id)
                       select mu;

            return linq.ToList();
        }

        public IList<Mission_User> GetByMission(int Id)
        {
            var linq = from mu in RepositoryContext.Mission_Users
                       where mu.MissionId.Equals(Id)
                       select mu;

            return linq.ToList();
        }

        public IList<Mission_User> GetByMissionAndUser(int MissionId, int UserId)
        {
            var linq = from mu in RepositoryContext.Mission_Users
                       where mu.MissionId.Equals(MissionId) && mu.UserId.Equals(UserId)
                       select mu;

            return linq.ToList();
        }
    }
}

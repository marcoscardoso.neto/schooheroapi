﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class HistoryRepository : BaseRepository<History>, IHistoryRepository
    {
        public HistoryRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public bool CreateHistory(string Action, string Reason)
        {
            History model = new History()
            {
                Action = Action,
                Date = DateTime.Now,
                Reason = Reason
            };

            var result = RepositoryContext.History.Add(model);

            RepositoryContext.SaveChanges();

            if (result.Entity != null)
                return true;
            else
                return false;
        }
    }
}

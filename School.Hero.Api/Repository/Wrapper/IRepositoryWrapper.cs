﻿using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Wrapper
{
    public interface IRepositoryWrapper
    {
        IUserRepository User { get; }
        IPermissionRepository Permission { get; }
        IMissionRepository Mission { get; }
        IShopRepository Shop{ get; }
        IUser_PermissionRepository User_Permission { get; }
        IUser_ShopRepository User_Shop { get; }
        IMission_UserRepository Mission_User { get; }
        IHistoryRepository History { get; }
    }
}

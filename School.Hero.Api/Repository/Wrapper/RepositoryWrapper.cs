﻿using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Wrapper
{
    public class RepositoryWrapper : IRepositoryWrapper
    {

        public RepositoryWrapper(SchoolHeroContext context)
        {
            _repoContext = context;
        }

        private SchoolHeroContext _repoContext;
        private IUserRepository _user;
        private IPermissionRepository _permission;
        private IMissionRepository _mission;
        private IShopRepository _shop;
        private IUser_PermissionRepository _user_permission;
        private IUser_ShopRepository _user_shop;
        private IMission_UserRepository _mission_user;
        private IHistoryRepository _history;

        public IUserRepository User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_repoContext);
                }

                return _user;
            }
        }
        public IPermissionRepository Permission
        {
            get
            {
                if (_permission == null)
                {
                    _permission = new PermissionRepository(_repoContext);
                }

                return _permission;
            }
        }
        public IMissionRepository Mission
        {
            get
            {
                if (_mission == null)
                {
                    _mission = new MissionRepository(_repoContext);
                }

                return _mission;
            }
        }
        public IShopRepository Shop
        {
            get
            {
                if (_shop == null)
                {
                    _shop = new ShopRepository(_repoContext);
                }

                return _shop;
            }
        }
        public IUser_PermissionRepository User_Permission
        {
            get
            {
                if (_user_permission == null)
                {
                    _user_permission = new User_PermissionRepository(_repoContext);
                }

                return _user_permission;
            }
        }
        public IUser_ShopRepository User_Shop
        {
            get
            {
                if (_user_shop == null)
                {
                    _user_shop = new User_ShopRepository(_repoContext);
                }

                return _user_shop;
            }
        }
        public IMission_UserRepository Mission_User
        {
            get
            {
                if (_mission_user == null)
                {
                    _mission_user = new Mission_UserRepository(_repoContext);
                }

                return _mission_user;
            }
        }

        public IHistoryRepository History
        {
            get
            {
                if (_history == null)
                {
                    _history = new HistoryRepository(_repoContext);
                }

                return _history;
            }
        }

    }
}

﻿using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace School.Hero.Api.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected SchoolHeroContext RepositoryContext { get; set; }

        public BaseRepository(SchoolHeroContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }

        public IEnumerable<T> GetAll()
        {
            return this.RepositoryContext.Set<T>();
        }

        public IEnumerable<T> GetByCondition(Expression<Func<T, bool>> expression)
        {
            return this.RepositoryContext.Set<T>().Where(expression);
        }

        public void Insert(T entity)
        {
            this.RepositoryContext.Set<T>().Add(entity);
            Save();
        }

        public void Update(T entity)
        {
            this.RepositoryContext.Set<T>().Update(entity);
            Save();
        }

        public void Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
            Save();
        }

        public void Save()
        {
            this.RepositoryContext.SaveChanges();
        }
    }
}

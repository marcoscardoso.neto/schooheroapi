﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IHistoryRepository : IBaseRepository<History>
    {
        bool CreateHistory(string Action, string Reason = "");
    }
}

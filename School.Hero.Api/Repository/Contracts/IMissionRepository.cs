﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IMissionRepository : IBaseRepository<Mission>
    {
        Mission GetById(int Id);
        IList<Mission> GetByIds(List<int> Ids);
    }
}

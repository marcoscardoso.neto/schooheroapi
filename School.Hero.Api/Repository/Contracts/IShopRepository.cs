﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IShopRepository : IBaseRepository<Shop>
    {
        Shop GetById(int Id);
        IList<Shop> GetByIds(List<int> Ids);
    }
}

﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetById(int Id);
        User Login(string UserName, string Password);
        User GetByUserName(string UserName);
        IList<User> GetByIds(List<int> Ids);
    }
}

﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IMission_UserRepository : IBaseRepository<Mission_User>
    {
        Mission_User GetById(int Id);
        IList<Mission_User> GetByUser(int Id);
        IList<Mission_User> GetByMission(int Id);
        IList<Mission_User> GetByMissionAndUser(int MissionId, int UserId);
    }
}

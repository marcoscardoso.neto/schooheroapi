﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IUser_PermissionRepository : IBaseRepository<User_Permission>
    {
        User_Permission GetById(int Id);
        User_Permission GetByUserId(int UserId);
    }
}

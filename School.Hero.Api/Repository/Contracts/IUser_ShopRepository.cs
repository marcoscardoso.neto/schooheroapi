﻿using School.Hero.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository.Contracts
{
    public interface IUser_ShopRepository : IBaseRepository<User_Shop>
    {
        User_Shop GetById(int Id);

        IList<User_Shop> GetByUser(int Id);

        IList<User_Shop> GetByShop(int Id);
    }
}

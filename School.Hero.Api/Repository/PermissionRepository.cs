﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class PermissionRepository : BaseRepository<Permission>, IPermissionRepository
    {
        public PermissionRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public Permission GetById(int Id)
        {
            return RepositoryContext.Permissions.Find(Id);
        }
    }
}

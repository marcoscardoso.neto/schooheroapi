﻿using School.Hero.Api.Models;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Repository
{
    public class User_ShopRepository : BaseRepository<User_Shop> , IUser_ShopRepository
    {
        public User_ShopRepository(SchoolHeroContext repositoryContext) : base(repositoryContext)
        {
        }

        public User_Shop GetById(int Id)
        {
            return RepositoryContext.User_Shops.Find(Id);
        }

        public IList<User_Shop> GetByUser(int Id)
        {
            var linq = from us in RepositoryContext.User_Shops
                       where us.UserId.Equals(Id)
                       select us;

            return linq.ToList();

        }

        public IList<User_Shop> GetByShop(int Id)
        {
            var linq = from us in RepositoryContext.User_Shops
                       where us.ShopId.Equals(Id)
                       select us;

            return linq.ToList();
        }
    }
}

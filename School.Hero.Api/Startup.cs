﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using School.Hero.Api.Extensions;
using School.Hero.Api.Models.Context;
using School.Hero.Api.Repository.Wrapper;
using Swashbuckle.AspNetCore.Swagger;

namespace School.Hero.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //configuração do Cors
            services.ConfigureCors();

            //configuração do IIS
            services.ConfigureIISIntegration();

            //Contexto de base de dados
            services.AddDbContext<SchoolHeroContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:SchoolHeroDB"]));

            //Faz a injeção de dependencia do repositoryWrapper
            services.ConfigureRepositoryWrapper();

            //Compatibilidade de versão
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.ConfigureSwagger();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("CorsPolicy");

            //Encaminhará cabeçalhos de proxy para a solicitação atual. 
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });

            //Apontará na página de índice no projeto Angular.
            app.Use(async (context, next) =>
            {
                await next();

                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            //ativa o uso de arquivos estáticos para a solicitação.
            //Se não definirmos um caminho para os arquivos estáticos, ele usará uma pasta wwwroot em nosso explorador de soluções por padrão.
            app.UseStaticFiles();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SchoolHero API V1");
                c.RoutePrefix = "api/v1";
            });

            app.UseMvc();
        }

    }
}

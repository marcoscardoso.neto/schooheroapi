﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace School.Hero.Api.Migrations
{
    public partial class add_User_deybson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 15, 13, 798, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 15, 13, 800, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 15, 13, 800, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 15, 13, 800, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 15, 13, 800, DateTimeKind.Local));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Avatar", "Coins", "Experience", "FullName", "IsActive", "Password", "Username" },
                values: new object[] { 4, "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-12-512.png", 1000, 600, "Deybson Ferreira", true, "123456", "Deybson" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 593, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));
        }
    }
}

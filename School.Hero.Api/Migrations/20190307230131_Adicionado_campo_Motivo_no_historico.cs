﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace School.Hero.Api.Migrations
{
    public partial class Adicionado_campo_Motivo_no_historico : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "History",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 3, 7, 20, 1, 31, 150, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 3, 7, 20, 1, 31, 153, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 3, 7, 20, 1, 31, 153, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 3, 7, 20, 1, 31, 153, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 3, 7, 20, 1, 31, 153, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reason",
                table: "History");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 377, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));
        }
    }
}

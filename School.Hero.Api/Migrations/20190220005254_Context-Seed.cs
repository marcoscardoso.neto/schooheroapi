﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace School.Hero.Api.Migrations
{
    public partial class ContextSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Picture",
                table: "Users",
                newName: "Avatar");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 19, 21, 52, 53, 720, DateTimeKind.Local));

            migrationBuilder.InsertData(
                table: "Missions",
                columns: new[] { "MissionId", "DateCreation", "Description", "Experience", "IsActive", "IsCompleted", "Name", "Value" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 2, 19, 21, 52, 53, 729, DateTimeKind.Local), "Esta Missao é apenas de Teste", 235, true, false, "Missao Teste 2", 90 },
                    { 3, new DateTime(2019, 2, 19, 21, 52, 53, 729, DateTimeKind.Local), "Esta Missao é apenas de Teste", 500, true, false, "Missao Teste 3", 70 }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "Avatar",
                value: "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-20-512.png");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2,
                column: "Avatar",
                value: "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-01-512.png");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserId", "Avatar", "Coins", "FullName", "IsActive", "Password", "Username" },
                values: new object[] { 3, "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-12-512.png", 10, "Joao da Silva", true, "123456", "Joao" });

            migrationBuilder.InsertData(
                table: "User_Permissions",
                columns: new[] { "UserPermissionId", "PermissionId", "UserId" },
                values: new object[] { 3, 2, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "User_Permissions",
                keyColumn: "UserPermissionId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 3);

            migrationBuilder.RenameColumn(
                name: "Avatar",
                table: "Users",
                newName: "Picture");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 19, 21, 24, 7, 164, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "Picture",
                value: null);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2,
                column: "Picture",
                value: null);
        }
    }
}

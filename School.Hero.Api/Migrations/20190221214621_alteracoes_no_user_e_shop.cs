﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace School.Hero.Api.Migrations
{
    public partial class alteracoes_no_user_e_shop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Experience",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Shops",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 593, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local));

            migrationBuilder.InsertData(
                table: "Shops",
                columns: new[] { "ShopId", "DateCreation", "Description", "IsActive", "Name", "Value" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local), "Este item te dara um ponto na matéria de programação", true, "1 Ponto na matéria de programação", 1000 },
                    { 2, new DateTime(2019, 2, 21, 18, 46, 21, 597, DateTimeKind.Local), "500 de XP", true, "Experiência", 1000 }
                });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 1,
                column: "Experience",
                value: 500);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 2,
                column: "Experience",
                value: 1000);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "UserId",
                keyValue: 3,
                column: "Experience",
                value: 100);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2);

            migrationBuilder.DropColumn(
                name: "Experience",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Shops");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 19, 21, 52, 53, 720, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 19, 21, 52, 53, 729, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 19, 21, 52, 53, 729, DateTimeKind.Local));
        }
    }
}

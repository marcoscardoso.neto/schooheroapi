﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace School.Hero.Api.Migrations
{
    public partial class Add_History_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "History",
                columns: table => new
                {
                    HistoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Action = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_History", x => x.HistoryId);
                });

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 377, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 28, 21, 1, 31, 380, DateTimeKind.Local));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "History");

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 21, 50, 790, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 21, 50, 793, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Missions",
                keyColumn: "MissionId",
                keyValue: 3,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 21, 50, 793, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 1,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 21, 50, 793, DateTimeKind.Local));

            migrationBuilder.UpdateData(
                table: "Shops",
                keyColumn: "ShopId",
                keyValue: 2,
                column: "DateCreation",
                value: new DateTime(2019, 2, 21, 19, 21, 50, 793, DateTimeKind.Local));
        }
    }
}

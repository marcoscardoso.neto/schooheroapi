﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.Context
{
    public class SchoolHeroContext : DbContext
    {
        public SchoolHeroContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Mission_User> Mission_Users { get; set; }
        public DbSet<User_Permission> User_Permissions { get; set; }
        public DbSet<User_Shop> User_Shops { get; set; }
        public DbSet<History> History { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Permission
            modelBuilder.Entity<Permission>().HasData(
                    new Permission
                    {
                        PermissionId = 1,
                        Description = "Teacher",
                    },
                    new Permission
                    {
                        PermissionId = 2,
                        Description = "Student",
                    }
                    );
            #endregion

            #region User
            modelBuilder.Entity<User>().HasData(
                    new User
                    {
                        UserId = 1,
                        Username = "Facal",
                        FullName = "Marcos Cardoso de Oliveira Neto",
                        Coins = 1000,
                        Password = "123456",
                        IsActive = true,
                        Avatar = "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-20-512.png",
                        Experience = 500
                    },
                    new User
                    {
                        UserId = 2,
                        Username = "Ducatti",
                        FullName = "Marcos Ducatti",
                        Coins = 1000,
                        Password = "123456",
                        IsActive = true,
                        Avatar = "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-01-512.png",
                        Experience = 1000
                    },
                    new User
                    {
                        UserId = 3,
                        Username = "Joao",
                        FullName = "Joao da Silva",
                        Coins = 10,
                        Password = "123456",
                        IsActive = true,
                        Avatar = "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-12-512.png",
                        Experience = 100
                    },
                    new User
                    {
                        UserId = 4,
                        Username = "Deybson",
                        FullName = "Deybson Ferreira",
                        Coins = 1000,
                        Password = "123456",
                        IsActive = true,
                        Avatar = "https://cdn1.iconfinder.com/data/icons/zeshio-s-fantasy-avatars/200/Fantasy_avatar_people-12-512.png",
                        Experience = 600
                    }
                    );
            #endregion

            #region User Permission
            modelBuilder.Entity<User_Permission>().HasData(
                new User_Permission
                {
                    UserPermissionId = 1,
                    PermissionId = 1,
                    UserId = 1
                },
                new User_Permission
                {
                    UserPermissionId = 2,
                    PermissionId = 1,
                    UserId = 2
                },
                new User_Permission
                {
                    UserPermissionId = 3,
                    PermissionId = 2,
                    UserId = 3
                }
                );
            #endregion

            #region Mission
            modelBuilder.Entity<Mission>().HasData(
                new Mission
                {
                    MissionId = 1,
                    DateCreation = DateTime.Now,
                    Name = "Missao Teste 1",
                    Description = "Esta Missao é apenas de Teste",
                    Experience = 100,
                    IsActive = true,
                    IsCompleted = false,
                    Value = 50
                },
                new Mission
                {
                    MissionId = 2,
                    DateCreation = DateTime.Now,
                    Name = "Missao Teste 2",
                    Description = "Esta Missao é apenas de Teste",
                    Experience = 235,
                    IsActive = true,
                    IsCompleted = false,
                    Value = 90
                },
                new Mission
                {
                    MissionId = 3,
                    DateCreation = DateTime.Now,
                    Name = "Missao Teste 3",
                    Description = "Esta Missao é apenas de Teste",
                    Experience = 500,
                    IsActive = true,
                    IsCompleted = false,
                    Value = 70
                }
                );
            #endregion

            #region Shop
            modelBuilder.Entity<Shop>().HasData(
                    new Shop
                    {
                        ShopId = 1,
                        DateCreation = DateTime.Now,
                        Name = "1 Ponto na matéria de programação",
                        Description = "Este item te dara um ponto na matéria de programação",
                        IsActive = true,
                        Value = 1000

                    },
                    new Shop
                    {
                        ShopId = 2,
                        DateCreation = DateTime.Now,
                        Name = "Experiência",
                        Description = "500 de XP",
                        IsActive = true,
                        Value = 1000
                    }
                    );
            #endregion
        }
    }
}

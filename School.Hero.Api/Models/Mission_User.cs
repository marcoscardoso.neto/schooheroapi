﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models
{
    [Table("Mission_Users")]
    public class Mission_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MissionUserId { get; set; }

        //Convenções
        public int UserId { get; set; }
        public User User { get; set; }

        public int MissionId { get; set; }
        public Mission Mission { get; set; }
    }
}

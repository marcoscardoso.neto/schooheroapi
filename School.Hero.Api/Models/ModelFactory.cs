﻿using School.Hero.Api.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models
{
    public class ModelFactory
    {
        #region User
        public UserModel Convert(User model)
        {
            return new UserModel()
            {
                UserId = model.UserId,
                Username = model.Username,
                FullName = model.FullName,
                Avatar = model.Avatar,
                Coins = model.Coins,
                Experience = model.Experience
            };
        }

        public IList<UserModel> Convert(IList<User> model)
        {
            return model.Select(x => new UserModel()
            {
                UserId = x.UserId,
                Username = x.Username,
                FullName = x.FullName,
                Avatar = x.Avatar,
                Coins = x.Coins,
                Experience = x.Experience
            }).ToList();
        }

        public User Convert(UserModel model)
        {
            return new User()
            {
                UserId = model.UserId,
                Username = model.Username,
                FullName = model.FullName,
                Avatar = model.Avatar,
                Coins = model.Coins,
                Experience = model.Experience
            };
        }

        public IList<User> Convert(IList<UserModel> model)
        {
            return model.Select(x => new User()
            {
                UserId = x.UserId,
                Username = x.Username,
                FullName = x.FullName,
                Avatar = x.Avatar,
                Coins = x.Coins,
                Experience = x.Experience
            }).ToList();
        }

        public void Merge(ref User modelData, UserModel modelView)
        {
            modelData.Username = modelView.Username;
            modelData.FullName = modelView.FullName;
            modelData.Avatar = modelView.Avatar;
            modelData.Coins = modelView.Coins;
            modelData.Experience = modelView.Experience;
        }

        #endregion

        #region Mission
        public MissionModel Convert(Mission model)
        {
            return new MissionModel()
            {
                MissionId = model.MissionId,
                Description = model.Description,
                Experience = model.Experience,
                Name = model.Name,
                Value = model.Value
            };
        }

        public IList<MissionModel> Convert(IList<Mission> model)
        {
            return model.Select(x => new MissionModel()
            {
                MissionId = x.MissionId,
                Description = x.Description,
                Experience = x.Experience,
                Name = x.Name,
                Value = x.Value
            }).ToList();
        }

        public Mission Convert(MissionModel model)
        {
            return new Mission()
            {
                MissionId = model.MissionId,
                Description = model.Description,
                Experience = model.Experience,
                Name = model.Name,
                Value = model.Value
            };
        }

        public IList<Mission> Convert(IList<MissionModel> model)
        {
            return model.Select(x => new Mission()
            {
                MissionId = x.MissionId,
                Description = x.Description,
                Experience = x.Experience,
                Name = x.Name,
                Value = x.Value
            }).ToList();
        }

        public void Merge(ref Mission modelData, MissionModel modelView)
        {
            modelData.Description = modelView.Description;
            modelData.Experience = modelView.Experience;
            modelData.Name = modelView.Name;
            modelData.Value = modelView.Value;
        }

        #endregion

        #region Shop
        public ShopModel Convert(Shop model)
        {
            return new ShopModel()
            {
                ShopId = model.ShopId,
                Description = model.Description,
                Name = model.Name,
                Value = model.Value
            };
        }

        public IList<ShopModel> Convert(IList<Shop> model)
        {
            return model.Select(x => new ShopModel()
            {
                ShopId = x.ShopId,
                Description = x.Description,
                Name = x.Name,
                Value = x.Value
            }).ToList();
        }

        public Shop Convert(ShopModel model)
        {
            return new Shop()
            {
                ShopId = model.ShopId,
                Description = model.Description,
                Name = model.Name,
                Value = model.Value
            };
        }

        public IList<Shop> Convert(IList<ShopModel> model)
        {
            return model.Select(x => new Shop()
            {
                ShopId = x.ShopId,
                Description = x.Description,
                Name = x.Name,
                Value = x.Value
            }).ToList();
        }

        public void Merge(ref Shop modelData, ShopModel modelView)
        {
            modelData.Description = modelView.Description;
            modelData.Name = modelView.Name;
            modelData.Value = modelView.Value;
        }

        #endregion

        #region Permission
        public PermissionModel Convert(Permission model)
        {
            return new PermissionModel()
            {
                PermissionId = model.PermissionId,
                Description = model.Description                
            };
        }

        public IList<PermissionModel> Convert(IList<Permission> model)
        {
            return model.Select(x => new PermissionModel()
            {
                PermissionId = x.PermissionId,
                Description = x.Description
            }).ToList();
        }

        public Permission Convert(PermissionModel model)
        {
            return new Permission()
            {
                PermissionId = model.PermissionId,
                Description = model.Description
            };
        }

        public IList<Permission> Convert(IList<PermissionModel> model)
        {
            return model.Select(x => new Permission()
            {
                PermissionId = x.PermissionId,
                Description = x.Description
            }).ToList();
        }
        public void Merge(ref Permission modelData, PermissionModel modelView)
        {
            modelData.Description = modelView.Description;
        }

        #endregion

    }
}

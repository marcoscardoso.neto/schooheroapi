﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public int Coins { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public string FullName { get; set; }
        public string Avatar { get; set; }
        [Required]
        public int Experience { get; set; }

        //Convenções
        public List<Mission_User> MissionUsers { get; set; }
        public List<User_Shop> UsersShop { get; set; }
        public List<User_Permission> UserPermissions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class PermissionModel
    {
        public int PermissionId { get; set; }
        [Required]
        public string Description { get; set; }
    }
}

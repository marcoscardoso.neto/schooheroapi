﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class MissionModel
    {
        public int MissionId { get; set; }
        [Required]
        [Display(Name = "Nome")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Descrição")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Moedas")]
        public int Value { get; set; }
        [Required]
        [Display(Name = "Experiência")]
        public int Experience { get; set; }

        public bool IsCompleted { get; set; }
    }
}

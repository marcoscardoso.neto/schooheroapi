﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class UserModel
    {
        public int UserId { get; set; }
        [Required]
        [Display(Name = "Nome de Usuario")]
        public string Username { get; set; }
        [Required]
        [Display(Name = "Moedas")]
        public int Coins { get; set; }
        [Required]
        [Display(Name = "Nome Completo")]
        public string FullName { get; set; }
        [Display(Name = "Avatar")]
        public string Avatar { get; set; }
        [Required]
        [Display(Name = "Experiência")]
        public int Experience { get; set; }
    }
}

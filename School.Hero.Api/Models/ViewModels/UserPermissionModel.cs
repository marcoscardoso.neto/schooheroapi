﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class UserPermissionModel
    {
        public int UserId { get; set; }
        public PermissionModel Permission { get; set; }
    }
}

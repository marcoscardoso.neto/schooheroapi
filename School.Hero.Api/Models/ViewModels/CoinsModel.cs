﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class CoinsModel
    {
        [Required]
        [Display(Name = "Moedas")]
        public int Coins { get; set; }
        [Required]
        [Display(Name = "Motivo")]
        public string Reason { get; set; }
    }
}

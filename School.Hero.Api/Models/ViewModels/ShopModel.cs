﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models.ViewModels
{
    public class ShopModel
    {
        public int ShopId { get; set; }
        [Required]
        [Display(Name = "Descrição do Item")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Nome do Item")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Valor do Item")]
        public int Value { get; set; }
    }
}

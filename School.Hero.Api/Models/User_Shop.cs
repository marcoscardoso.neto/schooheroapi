﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace School.Hero.Api.Models
{
    [Table("Users_Shop")]
    public class User_Shop
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserShopId { get; set; }

        //Convenções
        public int UserId { get; set; }
        public User User { get; set; }

        public int ShopId { get; set; }
        public Shop Shop { get; set; }
    }
}
